# Issue Reporter
I think the best way to describe what this project is about is to walk you through a common scenario. The scenario is where 10 users calls your backend service. Something goes wrong in one of the backend services and an error is returned to the user. You have some kind of alerting system configured that alerts you that your backend service returned an error. But the problem now is that you have 10 errors and the only way for you to see if they are related to each other is to lookup the logs for all 10 and compare them. This doable in a small system with few users. But when the system gets bigger and the number of users increases this becomes more and more time consuming.

This project is POC on how to solve this issue. The thinking is to trace the requests through all services and when an error occurs in one of the services that error will be logged along with its stacktrace. The error will then bubble up to the calling service who in turn will log the error (and its stacktrace) and this bubbling up will continue until we hit the root service. We will then collect all the stacktraces that have been generated for each service and create a hash of them. This hash will represent the code path from the first service to the code path in the last service where the error occured. So in other words this hash will create an unique id for this error. We can then create an issue in gitlab with this hash. When the next request comes in and triggers the same error that error will be grouped with the same issue inside gitlab.