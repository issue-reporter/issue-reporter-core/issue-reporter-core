package com.gitlab.soshibby.issuereporter.core.configuration;

import com.gitlab.soshibby.issuereporter.core.configuration.plugins.IssueBuilderConfig;
import com.gitlab.soshibby.issuereporter.core.configuration.plugins.IncidentBuilderConfig;
import com.gitlab.soshibby.issuereporter.core.parsers.ConfigParser;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginDefinition;
import org.springframework.stereotype.Component;

@Component
public class Config {
    private ConfigParser configParser = new ConfigParser();
    private String CONFIG_FOLDER = "configs";

    public String getIssueConfigPath() {
        return CONFIG_FOLDER + "/issue-builder.yaml";
    }

    public String getIncidentConfigPath() {
        return CONFIG_FOLDER + "/incident-builder.yaml";
    }

    public String getLogAggregatorConfigPath() { return CONFIG_FOLDER + "/log-aggregator.yaml"; }

    public String getIssueWriterConfigPath() {
        return CONFIG_FOLDER + "/issue-writer.yaml";
    }

    public IssueBuilderConfig getIssueConfig() {
        String configPath = getIssueConfigPath();
        return configParser.parse(configPath, IssueBuilderConfig.class);
    }

    public IncidentBuilderConfig getIncidentConfig() {
        String configPath = getIncidentConfigPath();
        return configParser.parse(configPath, IncidentBuilderConfig.class);
    }

    public PluginDefinition getIssueWriterConfig() {
        String configPath = getIssueWriterConfigPath();
        return configParser.parse(configPath, PluginDefinition.class);
    }

    public PluginDefinition getLogAggregatorConfig() {
        String configPath = getLogAggregatorConfigPath();
        return configParser.parse(configPath, PluginDefinition.class);
    }
}
