package com.gitlab.soshibby.issuereporter.core.configuration.plugins;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginDefinition;

import java.util.ArrayList;
import java.util.List;

public class BodyConfig {
    @JsonProperty(value = "appenders")
    private List<PluginDefinition> messageAppenders = new ArrayList<>();

    public List<PluginDefinition> getMessageAppenders() {
        return messageAppenders;
    }

    public void setMessageAppenders(List<PluginDefinition> messageAppenders) {
        this.messageAppenders = messageAppenders;
    }
}
