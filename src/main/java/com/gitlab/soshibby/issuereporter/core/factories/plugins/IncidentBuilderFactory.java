package com.gitlab.soshibby.issuereporter.core.factories.plugins;

import com.gitlab.soshibby.issuereporter.core.configuration.plugins.IncidentBuilderConfig;
import com.gitlab.soshibby.issuereporter.core.types.IncidentBuilder;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginDefinition;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginResolver;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IncidentBuilderFactory {

    private PluginResolver pluginResolver;

    public IncidentBuilderFactory(PluginResolver pluginResolver) {
        this.pluginResolver = pluginResolver;
    }

    public IncidentBuilder create(IncidentBuilderConfig builderConfig) {
        IncidentBuilder incidentBuilder = new IncidentBuilder();

        resolveMessageAppenders(builderConfig.getBodyConfig().getMessageAppenders(), incidentBuilder.getBodyAppenders());

        return incidentBuilder;
    }

    private void resolveMessageAppenders(List<PluginDefinition> pluginDefinitions, List<MessageAppender> appenders) {
        pluginDefinitions.stream()
                .map(pluginDefinition -> pluginResolver.resolve(pluginDefinition, MessageAppender.class))
                .forEach(appenders::add);
    }
}
