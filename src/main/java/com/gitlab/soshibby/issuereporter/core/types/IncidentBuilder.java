package com.gitlab.soshibby.issuereporter.core.types;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class IncidentBuilder {
    private List<MessageAppender> bodyMessageAppenders = new ArrayList<>();

    public String createBody(LogStatement logTrigger, List<LogStatement> logStatements) {
        StringJoiner sb = new StringJoiner("");

        for (MessageAppender messageAppender : bodyMessageAppenders) {
            sb.add(messageAppender.createMessageFrom(logTrigger, logStatements));
        }

        return sb.toString();
    }

    public List<MessageAppender> getBodyAppenders() {
        return bodyMessageAppenders;
    }
}
