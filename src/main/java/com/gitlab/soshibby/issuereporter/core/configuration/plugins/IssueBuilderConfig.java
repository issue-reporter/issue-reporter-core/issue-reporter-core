package com.gitlab.soshibby.issuereporter.core.configuration.plugins;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IssueBuilderConfig {
    @JsonProperty(value = "header")
    private HeaderConfig headerConfig;

    @JsonProperty(value = "body")
    private BodyConfig bodyConfig;

    public HeaderConfig getHeaderConfig() {
        return headerConfig;
    }

    public void setHeaderConfig(HeaderConfig headerConfig) {
        this.headerConfig = headerConfig;
    }

    public BodyConfig getBodyConfig() {
        return bodyConfig;
    }

    public void setBodyConfig(BodyConfig bodyConfig) {
        this.bodyConfig = bodyConfig;
    }
}
