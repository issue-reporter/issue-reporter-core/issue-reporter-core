package com.gitlab.soshibby.issuereporter.core.configuration.plugins;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IncidentBuilderConfig {
    @JsonProperty(value = "body")
    private BodyConfig bodyConfig;

    public BodyConfig getBodyConfig() {
        return bodyConfig;
    }

    public void setBodyConfig(BodyConfig bodyConfig) {
        this.bodyConfig = bodyConfig;
    }
}
