package com.gitlab.soshibby.issuereporter.core.utils;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The HashUtil class is used for calculating the hash of all the stack traces in the logs for a certain request.
 * This is so we will get a unique hash that will uniquely identify each function call from start to where the exception
 * was thrown. This path will be unique for each issue that arises in the system.
 */
public class HashUtil {

    /*
    public static String createIssueHash(LogStatement logTrigger, List<LogStatement> logStatements) {
        logStatements.sort(Comparator.comparing(LogStatement::getDate));

        List<String> hashes = logStatements.stream()
                // We are only interested in logs with a stacktrace, remove all other log statements.
                .filter(logStatement -> logStatement.getStackTrace() != null && !logStatement.getStackTrace().isEmpty())
                // Create a hash of all stacktraces.
                .map(logStatement -> hash(logStatement.getStackTrace()))
                .collect(Collectors.toList());

        String combinedHashes = String.join("", hashes);
        return hash(combinedHashes);
    }
    */

    public static String createIssueHash(LogStatement logTrigger, List<LogStatement> logStatements) {
        String exceptionSpanId = logTrigger.getExceptionSpanId();

        if (exceptionSpanId == null) {
            throw new RuntimeException("No exception span id found on trigger log. Trace id: '" + logTrigger.getTraceId() + "'.");
        }

        Map<String, String> spans = getUniqueSpans(logStatements);
        List<String> spanFilter = getSpansInTree(exceptionSpanId, spans);

        List<LogStatement> filteredLogStatements = logStatements.stream()
                // We are only interested in logs with a stacktrace, remove all other log statements.
                .filter(logStatement -> logStatement.getStackTrace() != null && !logStatement.getStackTrace().isEmpty())
                // We are only interested in logs in the exception span tree.
                .filter(logStatement -> spanFilter.contains(logStatement.getSpanId()))
                // Sort the logs by date.
                .sorted(Comparator.comparing(LogStatement::getDate))
                .collect(Collectors.toList());

        // Create a hash of all stacktraces.
        List<String> hashes = filteredLogStatements.stream()
                .map(logStatement -> hash(logStatement.getStackTrace()))
                .collect(Collectors.toList());

        String combinedHashes = String.join("", hashes);
        return hash(combinedHashes);
    }

    private static List<String> getSpansInTree(String spanId, Map<String, String> spans) {
        if (spans.containsKey(spanId)) {
            String parentSpanId = spans.get(spanId);
            List<String> result = getSpansInTree(parentSpanId, spans);
            result.add(spanId);
            return result;
        } else {
            return new ArrayList<>();
        }
    }

    private static Map<String, String> getUniqueSpans(List<LogStatement> logStatements) {
        return logStatements.stream()
                .filter(logStatement -> logStatement.getSpanId() != null && !logStatement.getSpanId().isEmpty())
                .collect(Collectors.toMap(LogStatement::getSpanId, LogStatement::getParentSpanId, (log1, log2) -> log1));
    }

    private static String hash(String str) {
        return Hashing.sha256()
                .hashString(str, StandardCharsets.UTF_8)
                .toString();
    }
}
