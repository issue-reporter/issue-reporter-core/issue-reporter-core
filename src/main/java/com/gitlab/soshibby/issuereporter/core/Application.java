package com.gitlab.soshibby.issuereporter.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = "com")
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        log.info("Starting application");
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner run(ApplicationContext context) {
        return args -> {
            Handler handler;

            try {
                handler = context.getBean(Handler.class);
            } catch (Exception e) {
                log.error("Something went wrong when doing dependency injection.", e);
                throw new RuntimeException("Something went wrong when doing dependency injection.", e);
            }

            try {
                handler.init();
            } catch (Exception e) {
                log.error("Failed to initialize handler. \n\n\n\n", e);
                throw new RuntimeException("Failed to initialize handler.", e);
            }
        };
    }

}
