package com.gitlab.soshibby.issuereporter.core.parsers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;

public class ConfigParser {

    private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    public <T> T parse(String configPath, Class<T> clazz) {
        File configFile = new File(configPath);

        if (!configFile.exists()) {
            throw new RuntimeException("Couldn't find config file '" + configFile.getAbsolutePath() + "'.");
        }

        try {
            return mapper.readValue(configFile, clazz);
        } catch (IOException e) {
            throw new RuntimeException("Failed to parse config '" + configFile.getAbsolutePath() + "'. " + e.getMessage(), e);
        }
    }
}
