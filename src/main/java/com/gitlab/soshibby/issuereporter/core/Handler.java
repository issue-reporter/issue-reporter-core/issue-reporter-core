package com.gitlab.soshibby.issuereporter.core;

import com.gitlab.soshibby.issuereporter.core.types.IssueBuilder;
import com.gitlab.soshibby.issuereporter.core.types.IncidentBuilder;
import com.gitlab.soshibby.issuereporter.core.utils.HashUtil;
import com.gitlab.soshibby.issuereporter.issuewriter.IssueWriter;
import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.logaggregator.LogAggregator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

@Component
public class Handler {
    private static final Logger log = LoggerFactory.getLogger(Handler.class);

    private LogAggregator logAggregator;
    private IssueBuilder issueBuilder;
    private IncidentBuilder incidentBuilder;
    private IssueWriter issueWriter;

    public Handler(@Qualifier("ConfigDefinedLogAggregator") LogAggregator logAggregator,
                   @Qualifier("ConfigDefinedIssueBuilder") IssueBuilder issueBuilder,
                   @Qualifier("ConfigDefinedIncidentBuilder") IncidentBuilder incidentBuilder,
                   @Qualifier("ConfigDefinedIssueWriter") IssueWriter issueWriter) {
        this.logAggregator = logAggregator;
        this.issueBuilder = issueBuilder;
        this.incidentBuilder = incidentBuilder;
        this.issueWriter = issueWriter;
    }

    public void init() {
        logAggregator.start(this::handleLogs);
    }

    private void handleLogs(LogStatement logStatement) {
        try {
            validateLogStatement(logStatement);

            log.debug("Handling log statement: " + logStatement);

            // Get all logs matching the trace id.
            List<LogStatement> logs = logAggregator.getLogs(logStatement.getTraceId());
            Assert.notEmpty(logs, "No logs found for trace id: " + logStatement.getTraceId());
            logs.forEach(this::validateLogStatement);

            // Generate hash for this issue.
            String hashId = HashUtil.createIssueHash(logStatement, logs);
            log.info("Created issue hash '" + hashId + "'.");

            // Create issue.
            String rootHeader = issueBuilder.createHeader(logStatement, logs);
            String rootBody = issueBuilder.createBody(logStatement, logs);

            log.info("Sending issue with header '" + rootHeader + "' to issue writer.");
            issueWriter.writeIssue(hashId, rootHeader, rootBody);

            // Create incident.
            String childBody = incidentBuilder.createBody(logStatement, logs);

            log.info("Sending incident to issue writer.");
            issueWriter.writeIncident(hashId, childBody);
        } catch (Exception e) {
            log.error("An error occurred while handling log statement. " + logStatement);
        }
    }

    private void validateLogStatement(LogStatement logStatement) {
        Assert.notNull(logStatement, "Log statements is null.");
        Assert.hasText(logStatement.getTraceId(), "Trace id is null or empty. " + logStatement);
        Assert.notNull(logStatement.getDate(), "Date is null on log statement. " + logStatement);
        Assert.notNull(logStatement.getFields(), "Fields is null on log statement. " + logStatement);
        Assert.notEmpty(logStatement.getFields(), "Fields is empty on log statement. " + logStatement);
    }

}
