package com.gitlab.soshibby.issuereporter.core.factories.plugins;

import com.gitlab.soshibby.issuereporter.core.configuration.plugins.IssueBuilderConfig;
import com.gitlab.soshibby.issuereporter.core.types.IssueBuilder;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginDefinition;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginResolver;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IssueBuilderFactory {

    private PluginResolver pluginResolver;

    public IssueBuilderFactory(PluginResolver pluginResolver) {
        this.pluginResolver = pluginResolver;
    }

    public IssueBuilder create(IssueBuilderConfig builderConfig) {
        IssueBuilder issueBuilder = new IssueBuilder();

        resolveMessageAppenders(builderConfig.getHeaderConfig().getMessageAppenders(), issueBuilder.getHeaderAppenders());
        resolveMessageAppenders(builderConfig.getBodyConfig().getMessageAppenders(), issueBuilder.getBodyAppenders());

        return issueBuilder;
    }

    private void resolveMessageAppenders(List<PluginDefinition> pluginDefinitions, List<MessageAppender> appenders) {
        pluginDefinitions.stream()
                .map(pluginDefinition -> pluginResolver.resolve(pluginDefinition, MessageAppender.class))
                .forEach(appenders::add);
    }
}
