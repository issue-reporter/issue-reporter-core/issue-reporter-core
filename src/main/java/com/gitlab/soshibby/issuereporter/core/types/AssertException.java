package com.gitlab.soshibby.issuereporter.core.types;

public class AssertException extends RuntimeException {

    public AssertException(String message) {
        super(message);
    }
}
