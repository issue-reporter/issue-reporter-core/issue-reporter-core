package com.gitlab.soshibby.issuereporter.core.producers;

import com.gitlab.soshibby.issuereporter.core.configuration.Config;
import com.gitlab.soshibby.issuereporter.core.factories.plugins.IncidentBuilderFactory;
import com.gitlab.soshibby.issuereporter.core.factories.plugins.IssueBuilderFactory;
import com.gitlab.soshibby.issuereporter.core.types.IncidentBuilder;
import com.gitlab.soshibby.issuereporter.core.types.IssueBuilder;
import com.gitlab.soshibby.issuereporter.issuewriter.IssueWriter;
import com.gitlab.soshibby.issuereporter.logaggregator.LogAggregator;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanProducer {

    private static final Logger log = LoggerFactory.getLogger(BeanProducer.class);

    @Bean(name = "ConfigDefinedLogAggregator")
    public LogAggregator createLogAggregator(Config config, PluginResolver pluginResolver) {
        try {
            return pluginResolver.resolve(config.getLogAggregatorConfig(), LogAggregator.class);
        } catch (Exception e) {
            log.error("Failed to create log aggregator.", e);
            throw new RuntimeException("Failed to create log aggregator.", e);
        }
    }

    @Bean(name = "ConfigDefinedIssueBuilder")
    public IssueBuilder createIssueBuilder(Config config, IssueBuilderFactory issueBuilderFactory) {
        try {
            return issueBuilderFactory.create(config.getIssueConfig());
        } catch (Exception e) {
            log.error("Failed to create issue builder.", e);
            throw new RuntimeException("Failed to create issue builder.", e);
        }
    }

    @Bean(name = "ConfigDefinedIncidentBuilder")
    public IncidentBuilder createIncidentBuilder(Config config, IncidentBuilderFactory incidentBuilderFactory) {
        try {
            return incidentBuilderFactory.create(config.getIncidentConfig());
        } catch (Exception e) {
            log.error("Failed to create incident builder.", e);
            throw new RuntimeException("Failed to create incident builder.", e);
        }
    }

    @Bean(name = "ConfigDefinedIssueWriter")
    public IssueWriter createIssueWriter(Config config, PluginResolver pluginResolver) {
        try {
            return pluginResolver.resolve(config.getIssueWriterConfig(), IssueWriter.class);
        } catch (Exception e) {
            log.error("Failed to create issue writer.", e);
            throw new RuntimeException("Failed to create issue writer.", e);
        }
    }

}
